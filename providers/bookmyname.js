#!/usr/bin/env casperjs

var casper = require('casper').create();
var system = require('system');
var fs = require('fs');
var utils = require('utils');

/* check if bookmyname credentials have been set as env variables */
function checkBookmynameCredentials() {
  if (!casper.cli.has('domainname')) {
    console.error("--domainname is missing.");
    exit();
  }
  if (!casper.cli.has('domainid')) {
    console.error("--domainid is missing.");
    exit();
  }
  if (!casper.cli.has('zonefile')) {
    console.error("--zonefile is missing.");
    exit();
  }
  if (!casper.cli.has('id')) {
    console.error("--id is missing.");
    exit();
  }
  if (!casper.cli.has('password')) {
    console.error("--password is missing.");
    exit();
  }
  /* return credentials */
  return {
    did: casper.cli.get('domainid'),
    dname: casper.cli.get('domainname'),
    zfile: casper.cli.get('zonefile'),
    zdata: fs.read(casper.cli.get('zonefile')),
    id: casper.cli.get('id'),
    pwd: casper.cli.get('password')
  };
}

var cred = checkBookmynameCredentials();

casper.start('https://www.bookmyname.com/manager.cgi?cmd=gdp&did=' + cred.did + '&mode=1');

/* login */
casper.then(function () {
  /* fill the signin form */
  this.fill('form[name="account"]', {
    handle: cred.id,
    passwd: cred.pwd
  }, false);
  /* submit the form */
  this.click('form[name="account"] input[name="submit"]');
});
/* wait for signing in */
casper.waitForText("Déconnectez-vous");
/* fill the new zone file */
casper.then(function () {
  this.fill('form[name="rmail"]', {
    gdp_zonefile: cred.zdata
  }, true);
});
/* wait for confirmation text */
casper.waitForText("Votre demande de personnalisation de DNS sera prise en compte \nd'ici quelques minutes.", function () {
  this.echo('The domain ' + cred.dname + ' zone has been updated.');
});

casper.run();
